export const initializeBackgroundGradient = () => {
  const body = document.body;
  const backgroundGradientElem = document.querySelector(
    ".g-elements__background"
  );

  if (body.classList.contains("path-layout-builder-page")) {
    backgroundGradientElem.remove();
  }

  if (!backgroundGradientElem) {
    return;
  }

  const manualBgAnimation = () => {
    body.addEventListener("mousemove", (e) => {
      if (localStorage.getItem("reduce_animation") === "false") {
        let xCord = e.clientX;
        let yCord = e.clientY;

        let xPercent = Math.ceil(
          parseInt((xCord / window.innerWidth) * 100) * 0.86
        );
        let yPercent = Math.ceil(
          parseInt((yCord / window.innerHeight) * 100) * 0.93
        );

        gsap.to(backgroundGradientElem, {
          backgroundImage: backgroundGradient(xPercent, yPercent),
          duration: 10,
          delay: 0.1,
          ease: "circ.out",
        });
      }
    });
  };

  const backgroundGradient = (xPercent, yPercent) => {
    return `
        radial-gradient(at ${xPercent}% ${
      yPercent * 1.2
    }%, hsla(199, 100%, 55%, 0.7) 0, transparent 60%),
        radial-gradient(at ${-xPercent}% ${yPercent}%, hsla(0, 0%, 100%, 1) 0, transparent 100%),
        radial-gradient(at ${yPercent}% ${xPercent}%, hsla(315, 100%, 80%, 0.7) 0, transparent 60%),
        radial-gradient(at ${-xPercent}% ${yPercent}%, hsla(315, 100%, 90%, 1) 0, transparent 85%)`;
  };

  manualBgAnimation();
};
