import { initializeMotionFunctionality } from "./pause-motion.js";
import { initializeLanguageSwitcher } from "./language-switcher.js";
import { initializeDualColor } from "./dual-color.js";
import { initializeContentReveal } from "./content-reveal.js";
import { initializeBackgroundTransition } from "./background-transition.js";
import { initializeBackgroundGradient } from "./background-gradient.js";

import "./global.scss";

((Drupal) => {
  Drupal.behaviors.global = {
    attach: (context) => {
      once("global-actions", "body", context).forEach(() => {
        // @TODO remove component but a big part of the styling depends on the classes set here.
        initializeMotionFunctionality();
        // initializeLanguageSwitcher();
        initializeDualColor();
        initializeContentReveal();
        // @TODO remove components?
        // initializeBackgroundTransition();
        initializeBackgroundGradient();
      });
    },
  };
})(Drupal, once);
