export const initializeMotionFunctionality = () => {
  // Grab the prefers reduced media query.
  const mediaQuery = window.matchMedia("(prefers-reduced-motion: reduce)");
  const body = document.body;

  if (mediaQuery && !mediaQuery.matches) {
    const motionButton = document.querySelector(".site-header__motion");

    // Default to disabled motion.
    body.classList.remove("enable-motion");
    body.classList.add("disable-motion");

    const enableMotion = () => {
      localStorage.setItem("reduce_animation", "false");
      body.classList.remove("disable-motion");
      body.classList.add("enable-motion");

      motionButton.querySelector("span").innerText = "Pause Motion";
      motionButton.classList.remove("paused");
    };

    const disableMotion = () => {
      localStorage.setItem("reduce_animation", "true");
      body.classList.add("disable-motion");
      body.classList.remove("enable-motion");

      motionButton.querySelector("span").innerText = "Resume Motion";
      motionButton.classList.add("paused");
    };

    // On load check if we should enable motion or not
    localStorage.getItem("reduce_animation") === "false"
      ? enableMotion()
      : disableMotion();

    motionButton.addEventListener("click", () => {
      const animationEnabled =
        localStorage.getItem("reduce_animation") === "false";
      animationEnabled ? disableMotion() : enableMotion();
    });
  }
  else {
    localStorage.setItem("reduce_animation", "true");
    body.classList.add("disable-motion");
    body.classList.remove("enable-motion");
  }
};
