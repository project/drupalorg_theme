export const initializeBackgroundTransition = () => {
  const darkComponents = document.querySelectorAll(".dark-bg");
  const body = document.body;

  if (
    !darkComponents.length ||
    body.classList.contains("path-layout-builder-page")
  ) {
    return;
  }

  const options = {
    threshold: 0.9,
  };

  const backgroundClasses = {
    darkBlue: "bg-dark-blue",
  };

  const observer = new IntersectionObserver(([entry]) => {
    if (entry.isIntersecting) {
      body.classList.add(backgroundClasses.darkBlue);
    } else {
      body.classList.remove(backgroundClasses.darkBlue);
    }
  }, options);

  darkComponents.forEach((darkComponent) => {
    observer.observe(darkComponent);
  });
};
