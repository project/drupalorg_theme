export const initializeContentReveal = () => {
  const dataReveal = document.querySelectorAll("[data-reveal]");
  const options = {
    threshold: 0.6,
  };

  const isInViewport = (element, options) => {
    return new Promise((resolve) => {
      if (!element) {
        return reject(new Error("Specified element not found"));
      }

      if (element.querySelector(".btn")) {
        options.threshold = 0;
      }

      const observer = new IntersectionObserver(([entry]) => {
        if (
          entry.isIntersecting &&
          !localStorage.getItem("reduce_animation") != "true"
        ) {
          resolve(entry.isIntersecting);
          observer.unobserve(element);
        }
      }, options);
      observer.observe(element);
    });
  };

  const slideUp = [
    { opacity: 0, transform: "translateY(40%) skewY(2deg)" },
    { opacity: 1, transform: "translateY(0) skewY(0)" },
  ];

  const fadeIn = [{ opacity: 0 }, { opacity: 1 }];

  let revealTiming = {
    duration: 700,
    easing: "cubic-bezier(0, 0.615, 0.35, 0.9)",
    iterations: 1,
    delay: 200,
    fill: "forwards",
  };

  dataReveal.forEach((elem) => {
    isInViewport(elem, options).then(() => {
      elem.classList.add("js-revealed");
    });
  });
};
