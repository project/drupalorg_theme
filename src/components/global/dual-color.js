export const initializeDualColor = () => {
  const darkRight = document.querySelectorAll(".dark--right");
  const darkLeft = document.querySelectorAll(".dark--left");
  const darkLeftMobile = document.querySelectorAll(".dark--left--mobile");

  if (!darkRight.length && !darkLeft.length && !darkLeftMobile.length) {
    return;
  }

  let elementOptions = {
    "g-elements__globe": {
      placement: "bottom",
      height: 80,
      margin: 50,
      side: "right",
    },
  };

  // if (window.matchMedia("(max-width: 768px)").matches) {
  //   elementOptions = {
  //     ...elementOptions,
  //   };
  // } else {
  //   elementOptions = {
  //     ...elementOptions,
  //   };
  // }

  const getStartingTrigger = (element) => {
    let startingTrigger;
    const currentElemOptions = elementOptions[element];
    if (currentElemOptions.placement === "top") {
      startingTrigger = `${
        currentElemOptions.height + currentElemOptions.margin
      }px`;
    } else if (currentElemOptions.placement === "bottom") {
      startingTrigger = `${currentElemOptions.placement}-=${currentElemOptions.margin}px`;
    }
    return startingTrigger;
  };

  const getTriggerElement = (element) => {
    const currentElemOptions = elementOptions[element];
    if (currentElemOptions.side === "right") {
      return darkRight;
    } else if (currentElemOptions.side === "left") {
      return darkLeft;
    } else if (
      currentElemOptions.side === "left-mobile" &&
      window.matchMedia("(max-width: 768px)").matches
    ) {
      return darkLeftMobile;
    }
  };

  const getElementOptions = (identifier) => {
    const currentElemOptions = elementOptions[identifier];
    if (currentElemOptions) {
      return currentElemOptions;
    }

    return null;
  };

  // Left Elements
  // const headerLogo = gsap.utils.toArray(".site-logo__icon");
  const leftElements = [];

  if (darkLeft.length && leftElements.length) {
    leftElements.forEach((element) => {
      if (element.classList.contains("swap--light")) {
        element.style.display = "block";
      }
      element.querySelectorAll(".swap--light").forEach((swap) => {
        swap.style.display = "block";
      });
    });
  }

  // Right Elements
  // const headerCallout = gsap.utils.toArray(".site-header__btn");
  // const headerMotion = gsap.utils.toArray(".site-header__motion");
  const languageSwitcher = gsap.utils.toArray(".g-elements__globe");
  // const mobileLanguageSwitcher = gsap.utils.toArray(".site-header__globe");
  // const mobileMenu = gsap.utils.toArray(".site-header__mobile-menu");
  const rightElements = [...languageSwitcher];

  if (darkRight.length && rightElements.length) {
    rightElements.forEach((element) => {
      if (element.classList.contains("swap--light")) {
        element.style.display = "block";
      }
      element.querySelectorAll(".swap--light").forEach((swap) => {
        swap.style.display = "block";
      });
    });
  }

  // Outer Animation
  [...rightElements, ...leftElements].forEach((section) => {
    const elementIdentifier = section.classList[0];
    const elementOptions = getElementOptions(elementIdentifier);
    if (!elementOptions) {
      return;
    }

    const startingTrigger = getStartingTrigger(elementIdentifier);
    const triggerElement = getTriggerElement(elementIdentifier);

    triggerElement.forEach((sectionTrigger) => {
      let componentEntrance = gsap.timeline({
        scrollTrigger: {
          trigger: sectionTrigger,
          start: `top ${startingTrigger}`,
          end: `+=${elementOptions.height}`,
          scrub: true,
        },
        defaults: { ease: "none" },
        onComplete: function () {
          if (section.classList.contains("swap--dark")) {
            gsap.to(section, { yPercent: 100, duration: 0 });
          }
        },
      });

      let componentExit = gsap.timeline({
        scrollTrigger: {
          trigger: sectionTrigger,
          start: `bottom ${startingTrigger}`,
          end: `+=${elementOptions.height}`,
          scrub: true,
        },
        defaults: { ease: "none" },
      });

      const firstStartingY = section.classList.contains("swap--dark") ? 0 : 100;
      const firstEndingY = section.classList.contains("swap--dark") ? -100 : 0;

      componentEntrance.fromTo(
        section,
        { yPercent: firstStartingY },
        { yPercent: firstEndingY }
      );

      section.classList.contains("swap--dark")
        ? componentExit.to(section, { yPercent: 0 })
        : componentExit.to(section, { yPercent: -100 });
    });
  });

  const headerLogoInner = gsap.utils.toArray(".site-logo__icon .swap__inner");
  const headerCalloutInner = gsap.utils.toArray(
    ".site-header__btn .swap__inner"
  );
  const headerMotionInner = gsap.utils.toArray(
    ".site-header__motion .swap__inner"
  );
  const languageSwitcherInner = gsap.utils.toArray(
    ".g-elements__globe .swap__inner"
  );
  const mobileLanguageSwitcherInner = gsap.utils.toArray(
    ".site-header__globe .swap__inner"
  );
  const mobileMenuInner = gsap.utils.toArray(
    ".site-header__mobile-menu .swap__inner"
  );

  const leftElementsInner = [...headerLogoInner];
  const rightElementsInner = [
    ...headerCalloutInner,
    ...headerMotionInner,
    ...languageSwitcherInner,
    ...mobileLanguageSwitcherInner,
    ...mobileMenuInner,
  ];

  // Inner animation
  [...leftElementsInner, ...rightElementsInner].forEach((section) => {
    const elementIdentifier = section.parentNode.classList[0];
    const elementOptions = getElementOptions(elementIdentifier);
    if (!elementOptions) {
      return;
    }

    const startingTrigger = getStartingTrigger(elementIdentifier);
    const triggerElement = getTriggerElement(elementIdentifier);

    triggerElement.forEach((sectionTrigger) => {
      let componentEntrance = gsap.timeline({
        scrollTrigger: {
          trigger: sectionTrigger,
          start: `top ${startingTrigger}`,
          end: `+=${elementOptions.height}`,
          scrub: true,
        },
        defaults: { ease: "none" },
        onComplete: function () {
          if (section.classList.contains("swap--dark")) {
            gsap.to(section, { yPercent: -100, duration: 0 });
          }
        },
      });

      let componentExit = gsap.timeline({
        scrollTrigger: {
          trigger: sectionTrigger,
          start: `bottom ${startingTrigger}`,
          end: `+=${elementOptions.height}`,
          scrub: true,
        },
        defaults: { ease: "none" },
      });

      const firstStartingY = section.classList.contains("swap--dark")
        ? 0
        : -100;
      const firstEndingY = section.classList.contains("swap--dark") ? 100 : 0;

      componentEntrance.fromTo(
        section,
        { yPercent: firstStartingY },
        { yPercent: firstEndingY }
      );

      section.classList.contains("swap--dark")
        ? componentExit.to(section, { yPercent: 0 })
        : componentExit.to(section, { yPercent: 100 });
    });
  });
};
