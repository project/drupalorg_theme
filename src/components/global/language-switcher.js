export const initializeLanguageSwitcher = () => {
  const desktopLangSwitchButton = document.querySelectorAll(
    ".g-elements__globe-btn"
  );
  const mobileLangSwitchButton = document.querySelector(".site-header__globe");
  const languageCloseIcon = document.querySelector(
    ".g-elements__language-switcher-close"
  );

  if (!desktopLangSwitchButton.length || !languageCloseIcon) {
    return;
  }

  const body = document.body;
  const switchButtons = [...desktopLangSwitchButton, mobileLangSwitchButton];
  switchButtons.forEach((button) => {
    button.addEventListener("click", () => {
      if (body.classList.contains("language-expanded")) {
        body.classList.remove("language-expanded");
        switchButtons.forEach((button) => {
          button.setAttribute("aria-label", "Open Language Selector");
        });
        return;
      }
      body.classList.add("language-expanded");
      switchButtons.forEach((button) => {
        button.setAttribute("aria-label", "Close Language Selector");
      });
    });
  });

  languageCloseIcon.addEventListener("click", () => {
    if (body.classList.contains("language-expanded")) {
      body.classList.remove("language-expanded");
    }
  });
};
