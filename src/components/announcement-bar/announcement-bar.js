import { debounce } from "@scripts/utils/debounce";

export const initializeAnnouncementBar = (settings) => {
  const announcementBar = document.querySelector(".site-announcement");

  if (!announcementBar) {
    return;
  }

  // Show if we haven't seen it or it has changed since we saw it.
  let show = true;
  const lastViewedAt = window.localStorage.getItem("do-announce-viewed");
  if (lastViewedAt) {
    show = parseInt(announcementBar.dataset.changed) > parseInt(lastViewedAt);
  }

  const setAnnouncementBarHeight = debounce(() => {
    document.body.style.setProperty(
      "--announcement-bar-offset",
      `${announcementBar.clientHeight}px`
    );
  }, 100);

  if (show) {
    announcementBar.classList.remove("hidden");

    setAnnouncementBarHeight();
    window.addEventListener("resize", setAnnouncementBarHeight);
  }

  const closeIcon = announcementBar.querySelector(".site-announcement__close");

  closeIcon.addEventListener("click", (event) => {
    event.preventDefault();
    const barHeight = announcementBar.clientHeight;
    announcementBar.style.marginTop = `-${barHeight}px`;
    announcementBar.addEventListener("transitionend", () => {
      announcementBar.style.display = "none";
    });
    announcementBar.classList.add("dismissed");
    window.localStorage.setItem("do-announce-viewed", settings.request_time);
    window.removeEventListener("resize", setAnnouncementBarHeight);
    document.body.style.setProperty("--announcement-bar-offset", "");
  });
};
