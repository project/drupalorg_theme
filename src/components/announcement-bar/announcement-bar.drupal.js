import { initializeAnnouncementBar } from "./announcement-bar.js";
import "./announcement-bar.scss";

((Drupal) => {
  Drupal.behaviors.announcement_bar = {
    attach: (context, settings) => {
      once("announcement-bar", "body", context).forEach(() => {
        initializeAnnouncementBar(settings);
      });
    },
  };
})(Drupal);
