export const initializeResourceCards = () => {
  const resources = document.querySelector(".c-resources__cards.swiper");

  if (!resources) {
    return;
  }

  let cardSlider;
  const mediaQuery = window.matchMedia("(min-width: 768px)");
  const sliderSettings = {
    slidesPerView: 1.15,
    centeredSlides: true,
    spaceBetween: 20,
    keyboard: {
      enabled: true,
    },
    loop: false,
    rewind: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  };

  const initializeSlider = () => {
    cardSlider = new Swiper(resources, sliderSettings);
  };

  if (!mediaQuery.matches) {
    initializeSlider();
  }

  mediaQuery.addEventListener("change", (event) => {
    if (event.matches) {
      cardSlider.destroy();
    } else {
      initializeSlider();
    }
  });
};
