import { initializeResourceCards } from "./resource-cards.js";
import "./resource-cards.scss";

((Drupal) => {
  Drupal.behaviors.resource_cards = {
    attach: (context) => {
      once(
        "resource-cards",
        context.querySelectorAll(".c-resources"),
        context
      ).forEach(() => {
        initializeResourceCards();
      });
    },
  };
})(Drupal);
