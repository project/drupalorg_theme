export const initializeHero = () => {
  const heroComponent = document.querySelector(".c-hero");

  if (!heroComponent) {
    return;
  }
  const heroCta = document.querySelector(".c-hero__callout");
  const lastFragment = document.querySelector(
    ".c-hero__rows .c-hero__row:nth-child(3) > .c-hero__fragment:nth-child(3)"
  );
  if (!heroCta || !lastFragment) {
    return;
  }
  if (!lastFragment.querySelector(".c-hero__callout")) {
    const clonedCta = heroCta.cloneNode(true);
    clonedCta.classList.add("c-hero__callout--fragment");
    lastFragment.appendChild(clonedCta);
  }

  const globalCursor = document.querySelector(".g-elements__cursor-dot");

  if (!globalCursor) {
    return;
  }
  const heroImages = document.querySelectorAll(".c-hero .c-hero__image-link");

  heroImages.forEach((image) => {
    image.addEventListener("mouseover", () => {
      globalCursor.classList.add("active");
      TweenMax.fromTo(
        globalCursor,
        0.6,
        { scale: 0 },
        {
          scale: 1,
        }
      );
    });
    image.addEventListener("mouseout", () => {
      globalCursor.classList.remove("active");
      TweenMax.to(globalCursor, 0.3, {
        scale: 0,
      });
    });
  });

  const moveCursor = (e) => {
    TweenMax.to(globalCursor, 0.25, {
      x: e.clientX - 50,
      y: e.clientY - 50,
    });
  };
  window.addEventListener("mousemove", moveCursor);
};
