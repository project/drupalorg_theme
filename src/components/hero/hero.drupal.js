import { initializeHero } from "./hero.js";
import "./hero.scss";

((Drupal) => {
  Drupal.behaviors.hero = {
    attach: (context, settings) => {
      once("hero", context.querySelectorAll(".c-hero"), context).forEach(() => {
        initializeHero();
      });
    },
  };
})(Drupal);
