export const initializeWhyDrupal = () => {
  const whyDrupal = document.querySelector(".why-drupal");

  if (!whyDrupal) {
    return;
  }

  const slidesWrapper = whyDrupal.querySelector(".why-drupal__inner");
  const textSliderElem = whyDrupal.querySelector(
    ".why-drupal__text-slider > .swiper"
  );
  const imageSliderElem = whyDrupal.querySelector(
    ".why-drupal__image-slider > .swiper"
  );
  const textSlides = whyDrupal.querySelectorAll(
    ".why-drupal__text-slider div[data-slide]"
  );

  let imageSlider;
  let textSlider;

  const slideBullets = whyDrupal.querySelectorAll(".why-drupal__bullet");
  const mediaQuery = window.matchMedia("(min-width: 768px)");

  slideBullets.forEach((control) => {
    control.addEventListener("click", () => {
      const newSlideIdx = control.dataset.slide;
      if (newSlideIdx) {
        activateSlide(newSlideIdx);
        // control.classList.add("control-active");
      }
    });
  });

  const activateSlide = (newSlideIdx) => {
    // Remove active class from all text and image slides
    textSlides.forEach((slide) => {
      slide.classList.remove("slide-active");
    });
    // Add active slide classes to text and image slides
    const newSlides = slidesWrapper.querySelectorAll(
      `div[data-slide="${newSlideIdx}"]`
    );
    newSlides.forEach((newSlide) => {
      newSlide.classList.add("slide-active");
    });

    if (imageSlider && !imageSlider.destroyed) {
      imageSlider.slideTo(newSlideIdx - 1);
    }

    if (textSlider && !textSlider.destroyed) {
      textSlider.slideTo(newSlideIdx - 1);
    }

    // Remove active class from bullet
    slideBullets.forEach((control) => {
      control.classList.remove("control-active");
    });

    activateBullet(newSlideIdx);
  };

  const activateBullet = (slideIdx) => {
    const newBullet = slidesWrapper.querySelector(
      `.why-drupal__bullets button[data-slide="${slideIdx}"]`
    );
    if (newBullet && !newBullet.classList.contains("control-active")) {
      slideBullets.forEach((control) => {
        control.classList.remove("control-active");
      });
      newBullet.classList.add("control-active");
    }
  };

  const initImageSlider = () => {
    imageSlider = new Swiper(imageSliderElem, {
      mousewheel: false,
      speed: 700,
      watchSlidesProgress: true,
      parallax: true,
      allowTouchMove: true,
      lazy: {
        loadPrevNext: true,
      },
      breakpoints: {
        1024: {
          allowTouchMove: false,
        },
      },
    });

    imageSlider.on("slideChange", function () {
      if (mediaQuery.matches) {
        activateSlide(imageSlider.realIndex + 1);
      } else {
        activateBullet(imageSlider.realIndex + 1);
      }
    });
  };

  const initTextSlider = () => {
    textSlider = new Swiper(textSliderElem, {
      slideActiveClass: "slide-active",
    });
    textSlider.controller.control = imageSlider;
    imageSlider.controller.control = textSlider;
    textSlider.on("slideChangeTransitionEnd", function () {
      activateBullet(textSlider.realIndex + 1);
    });
  };

  initImageSlider();
  if (!mediaQuery.matches) {
    initTextSlider();
  }
  mediaQuery.addEventListener("change", (event) => {
    if (event.matches) {
      imageSlider.controller.control = null;
      textSlider.disable();
      textSlider.destroy();
      const activeBullet = slidesWrapper.querySelector(
        `.why-drupal__bullets .control-active`
      );
      if (activateBullet) {
        activateSlide(activeBullet.dataset.slide);
      }
    } else {
      initTextSlider();
    }
  });
};
