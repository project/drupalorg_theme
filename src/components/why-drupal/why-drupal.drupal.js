import { initializeWhyDrupal } from "./why-drupal.js";
import "./why-drupal.scss";

((Drupal) => {
  Drupal.behaviors.why_drupal = {
    attach: (context) => {
      once(
        "why-drupal",
        context.querySelectorAll(".why-drupal"),
        context
      ).forEach(() => {
        initializeWhyDrupal();
      });
    },
  };
})(Drupal);
