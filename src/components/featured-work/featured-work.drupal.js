import { initializeFeaturedWork } from "./featured-work.js";
import "./featured-work.scss";

((Drupal) => {
  Drupal.behaviors.featured_work = {
    attach: (context) => {
      once(
        "featured-work",
        context.querySelectorAll(".c-featured-work")
      ).forEach(() => {
        initializeFeaturedWork();
      });
    },
  };
})(Drupal);
