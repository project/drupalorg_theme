export const initializeFeaturedWork = () => {
  gsap.registerPlugin(ScrollTrigger);
  let animationEnabled = false;
  const items = gsap.utils.toArray(".c-featured-work__slide");

  const createTimelines = () => {
    items.forEach((showcase, i) => {
      showcase.classList.add("scrolltrigger-active");
      const slideOverlay = showcase.querySelector(
        ".c-featured-work__slide-overlay"
      );

      gsap.to(showcase, {
        scrollTrigger: {
          id: i === items.length - 1 ? "showcaseAnim" : `showcaseAnim-${i}`,
          trigger: showcase,
          scrub: true,
          pin: i === items.length - 1 ? false : true,
          pinSpacing: false,
          start: "top",
          end: "bottom",
          toggleClass: "active",
          ease: "power1",
        },
      });

      // Image animation
      gsap.to(showcase.querySelector("img"), {
        scrollTrigger: {
          id: `imageAnim-${i}`,
          trigger: showcase,
          scrub: true,
          start: "top top",
          end: "bottom -200%",
          ease: "power2",
        },
        y: "-10%",
        scale: 1.3,
        duration: 1,
      });

      // Overlay animation
      gsap.fromTo(
        slideOverlay,
        {
          autoAlpha: 0,
        },
        {
          scrollTrigger: {
            id: `overlayAnim-${i}`,
            trigger: slideOverlay,
            scrub: true,
            start: "top 50%",
            end: "bottom -200%",
            ease: "power2",
          },
          duration: 1,
          autoAlpha: 0.8,
        }
      );
    });
  };

  const destroyTimelines = () => {
    // Get last showcase item ScrollTrigger.
    const lastShowcaseItem = ScrollTrigger.getById("showcaseAnim");
    const animations = [lastShowcaseItem]; // All existing ScrollTriggers array.
    let triggerTargets = [];
    // Loop through showcase items and add dynamic ScrollTrigger objects to animations array.
    items.forEach((showcase, i) => {
      showcase.classList.remove("scrolltrigger-active");
      let showcaseItemAnimation = ScrollTrigger.getById(`showcaseAnim-${i}`);
      let imageAnimation = ScrollTrigger.getById(`imageAnim-${i}`);
      let overlayAnimation = ScrollTrigger.getById(`overlayAnim-${i}`);
      if (showcaseItemAnimation) {
        animations.push(showcaseItemAnimation);
      }
      if (imageAnimation) {
        animations.push(imageAnimation);
      }
      if (overlayAnimation) {
        animations.push(overlayAnimation);
      }
    });

    // Loop through existing featured work animations and kill them.
    animations.forEach((trigger) => {
      trigger.kill(true);
      let animation = trigger.animation;
      if (animation) {
        if (animation.getChildren) {
          animation.getChildren(true, true, false).forEach((child) => {
            triggerTargets = [...triggerTargets, ...child.targets()];
          });
        } else {
          triggerTargets = [...triggerTargets, ...animation.targets()];
        }
      }
    });

    gsap.set(triggerTargets, { clearProps: "all" });
  };

  // Detect class change on body element and create/destroy timelines based on class.
  const observer = new MutationObserver((mutations) => {
    mutations.forEach((mutation) => {
      if (
        mutation.type !== "attributes" &&
        mutation.attributeName !== "class"
      ) {
        return;
      }
      if (mutation.target.classList.contains("disable-motion")) {
        if (animationEnabled) {
          animationEnabled = false;
          destroyTimelines();
        }
      } else if (mutation.target.classList.contains("enable-motion")) {
        if (!animationEnabled) {
          animationEnabled = true;
          createTimelines();
        }
      }
    });
  });

  observer.observe(document.body, {
    attributes: true,
    attributeFilter: ["class"],
    childList: false,
    characterData: false,
  });
};
