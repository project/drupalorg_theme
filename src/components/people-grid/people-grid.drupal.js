import { initializePeopleGrid } from "./people-grid.js";
import "./people-grid.scss";

((Drupal) => {
  Drupal.behaviors.people_grid = {
    attach: (context) => {
      once(
        "people-grid",
        context.querySelectorAll(".people-grid"),
        context
      ).forEach(() => {
        initializePeopleGrid();
      });
    },
  };
})(Drupal);
