export const initializePeopleGrid = () => {
  const peopleGrid = document.querySelector(".people-grid");

  if (!peopleGrid) {
    return;
  }
  gsap.registerPlugin(ScrollTrigger);

  const firstRow = peopleGrid.querySelector(
    ".view-content .contributors__row--row1"
  );
  const secondRow = peopleGrid.querySelector(
    ".view-content .contributors__row--row2"
  );

  [firstRow, secondRow].forEach((row, idx) => {
    if (!row) {
      return;
    }
    idx++;
    const startingX = idx % 2 === 0 ? -250 : -100;
    const endingX = idx % 2 === 0 ? -450 : 100;
    const scroll = gsap.timeline({
      scrollTrigger: {
        trigger: peopleGrid,
        start: "top-=600",
        end: "bottom+=350",
        scrub: true,
      },
    });

    scroll.fromTo(
      row,
      {
        x: startingX,
      },
      {
        x: endingX,
        duration: 1,
        ease: "none",
        force3D: !0,
      }
    );
  });
};
