export const initializeHeader = () => {
  const siteHeader = document.querySelector(".site-header");

  if (!siteHeader) {
    return;
  }

  // Desktop switch breakpoint
  const sizeMq = window.matchMedia("(min-width: 1342px)");

  const searchTrigger = document.querySelector(".site-header__search");
  const headerClasses = {
    noDropdown: "site-header--no-dropdown",
    scrolled: "site-header--scrolled",
    scrolledAnimated: "site-header--scrolled-animated",
    sticky: "site-header--sticky",
  };

  const stickyPos = 400;
  const scrolledPos = stickyPos - 150;
  let transitionTimeout;
  let timer;

  let oldScroll = 0;
  const stickyHeader = () => {
    const scrollPos = window.scrollY;

    if (scrollPos > 0 && scrollPos < stickyPos) {
      siteHeader.classList.add(headerClasses.noDropdown);

      // Close search on initial scroll
      if (searchTrigger.classList.contains("is-expanded")) {
        document.body.classList.remove("search-expanded");
        searchTrigger.classList.remove("is-expanded");
      }
    } else {
      siteHeader.classList.remove(headerClasses.noDropdown);
    }

    if (scrollPos >= scrolledPos) {
      clearTimeout(transitionTimeout);
      siteHeader.classList.add(headerClasses.scrolled);
      transitionTimeout = setTimeout(() => {
        siteHeader.classList.add(headerClasses.scrolledAnimated);
      }, 100);
    } else {
      siteHeader.classList.remove(
        headerClasses.scrolled,
        headerClasses.scrolledAnimated
      );
      clearTimeout(transitionTimeout);
    }

    // if (scrollPos >= stickyPos) {
    //   siteHeader.classList.add(headerClasses.sticky);
    // } else {
    //   siteHeader.classList.remove(headerClasses.sticky);
    // }

    // Check if user is scrolling up and add sticky class.
    const isSticky = siteHeader.classList.contains(headerClasses.sticky);

    if (!isSticky && scrollPos >= scrolledPos && oldScroll > scrollPos) {
      siteHeader.classList.add(headerClasses.sticky);
    } else if (
      (isSticky && oldScroll < scrollPos) ||
      (isSticky && scrollPos <= scrolledPos)
    ) {
      siteHeader.classList.remove(headerClasses.sticky);
    }

    oldScroll = scrollPos;
  };

  window.addEventListener(
    "scroll",
    function () {
      if (timer) {
        this.cancelAnimationFrame(timer);
      }
      timer = this.requestAnimationFrame(stickyHeader);
    },
    false
  );

  stickyHeader();

  sizeMq.addEventListener("change", (event) => {
    siteHeader.classList.add("site-header--no-transition");

    // Remove no-transition class after timeout.
    setTimeout(() => {
      siteHeader.classList.remove("site-header--no-transition");
    }, 800);
  });
};
