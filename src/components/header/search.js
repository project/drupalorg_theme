export const initializeSearch = () => {
  // Search Functionality
  const searchTrigger = document.querySelector(".site-header__search");
  const mobileSearchTrigger = document.querySelector(".nav__search");

  const desktopSearchInput = document.querySelector(
    ".c-search.c-search--desktop input"
  );
  const mobileSearchInput = document.querySelector(
    ".c-search.c-search--mobile input"
  );

  if (!searchTrigger && !mobileSearchTrigger) {
    return;
  }

  // Body
  const body = document.body;

  // Desktop switch breakpoint
  const desktopSearchBp = window.matchMedia("(min-width: 1342px)");

  const searchClasses = {
    expandedState: "is-expanded",
    desktopExpanded: "search-expanded",
    mobileExpanded: "mobile-search-expanded",
  };

  const closeSearch = (trigger, searchClass) => {
    if (trigger.classList.contains(searchClasses.expandedState)) {
      body.classList.remove(searchClass);
      trigger.classList.remove(searchClasses.expandedState);
      trigger.setAttribute("aria-expanded", "false");
      trigger.setAttribute("aria-label", "Open Search Box");
      return true;
    }

    return false;
  };

  searchTrigger.addEventListener("click", () => {
    const searchClosed = closeSearch(
      searchTrigger,
      searchClasses.desktopExpanded
    );
    if (searchClosed) {
      return;
    }

    body.classList.add(searchClasses.desktopExpanded);
    searchTrigger.classList.add(searchClasses.expandedState);
    searchTrigger.setAttribute("aria-expanded", "true");
    searchTrigger.setAttribute("aria-label", "Close Search Box");
    setTimeout(() => {
      desktopSearchInput.focus();
    }, 250);
  });

  mobileSearchTrigger.addEventListener("click", () => {
    const searchClosed = closeSearch(
      mobileSearchTrigger,
      searchClasses.mobileExpanded
    );
    if (searchClosed) {
      return;
    }

    body.classList.add(searchClasses.mobileExpanded);
    mobileSearchTrigger.classList.add(searchClasses.expandedState);
    mobileSearchTrigger.setAttribute("aria-expanded", "true");
    mobileSearchTrigger.setAttribute("aria-label", "Close Search Box");
    setTimeout(() => {
      mobileSearchInput.focus();
    }, 250);
  });

  desktopSearchBp.addEventListener("change", (event) => {
    if (event.matches) {
      closeSearch(mobileSearchTrigger, searchClasses.mobileExpanded);
    } else {
      closeSearch(searchTrigger, searchClasses.desktopExpanded);
    }
  });

  // Check for forward/backward tabbing and if the input is empty.
  body.addEventListener("keydown", (e) => {
    if (e.key !== "Tab") {
      return;
    }

    if (
      body.classList.contains(searchClasses.desktopExpanded) &&
      desktopSearchInput.value.trim() == ""
    ) {
      closeSearch(searchTrigger, searchClasses.desktopExpanded);

      setTimeout(() => {
        searchTrigger.focus();
      }, 250);
      return;
    }

    if (
      body.classList.contains(searchClasses.mobileExpanded) &&
      mobileSearchInput.value.trim() == ""
    ) {
      closeSearch(mobileSearchTrigger, searchClasses.mobileExpanded);

      setTimeout(() => {
        mobileSearchTrigger.focus();
      }, 250);
      return;
    }
  });
};
