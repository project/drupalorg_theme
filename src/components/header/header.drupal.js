import { initializeHeader } from "./header.js";
import { initializeNav } from "./menu.js";
import { initializeSearch } from "./search.js";

import "./styles/nav-group-block.scss";
import "./styles/nav-callout.scss";
import "./styles/nav-footer.scss";
import "./styles/header.scss";
import "./styles/header-links.scss";
import "./styles/site-branding.scss";
import "./styles/menu.scss";
import "./styles/menu-mobile-header.scss";
import "./styles/menu-mobile-footer.scss";
import "./styles/menu-dropdown.scss";
import "./styles/menu-breadcrumbs.scss";
import "./styles/secondary-nav.scss";
import "./styles/search.scss";

((Drupal) => {
  Drupal.behaviors.header = {
    attach: (context) => {
      once("header", context.querySelector(".site-header"), context).forEach(
        () => {
          initializeHeader();
          initializeNav();
          initializeSearch();
        }
      );
    },
  };
})(Drupal);
