import { trapFocus } from '@scripts/utils/focus';

export const initializeNav = () => {
  // Main menu functionality
  const mainMenu = document.querySelector(".nav");

  if (!mainMenu) {
    return;
  }

  // Main menu mobile hamburger icon trigger (mobile).
  const hamburgerMenu = document.querySelector(".site-header__menu-trigger");

  // Main menu close icon trigger (mobile).
  const closeIcon = document.querySelector(".nav .nav__close-icon");

  // Main menu breadcrumbs wrapper (mobile).
  const menuBreadcrumbs = document.querySelector(".nav .nav__breadcrumbs");

  // Main menu breadcrumbs levels (mobile).
  const menuBreadcrumbsLevels = document.querySelectorAll(
    ".nav__breadcrumb-level button"
  );

  // Main menu dropdown carets (level-0) (mobile).
  const dropdownCarets = document.querySelectorAll(".nav .nav__link-caret");

  // Main menu dropdown carets (level-1) (mobile).
  const subCaretDropdown = document.querySelectorAll(
    ".nav .nav__dropdown-group-mobile-trigger"
  );

  // Body
  const body = document.body;

  // Trapped focus status
  let trapped;

  // Desktop switch breakpoint
  const sizeMq = window.matchMedia("(min-width: 1342px)");

  const bodyClasses = {
    mobileNavOpen: "nav-open",
    subMenuExpanded: "submenu-expanded",
    subMenuExpandedTransition: "submenu-expanded--transition",
  };

  // Reusable classes for menu functionality.
  const menuClasses = {
    levelZeroOpen: "menu-item--open",
    levelZeroOpenTransition: "menu-item--open-transition",
    levelOneOpen: "menu-item__dropdown--open",
    levelOneOpenTransition: "menu-item__dropdown--open-transition",
    subMenuGroupActive: "menu__subnav-block--open",
    breadcrumbActive: "breadcrumbs-active",
    breadcrumbActiveLevelOne: "breadcrumbs-active--level-1",
    breadcrumbActiveLevelTwo: "breadcrumbs-active--level-2",
    breadcrumbLevelOne: "nav__breadcrumbs-level-1",
    breadcrumbLevelTwo: "nav__breadcrumbs-level-2",
  };

  /**
   * Closes menu & removes all associated classes.
   */
  const closeMenu = () => {
    // Restore values to what it should be.
    if (localStorage.getItem("reduce_animation") === "true") {
      body.classList.remove("enable-motion");
      body.classList.add("disable-motion");
    }

    if (body.classList.contains(bodyClasses.mobileNavOpen)) {
      body.classList.remove(bodyClasses.mobileNavOpen);
      hamburgerMenu.setAttribute("aria-expanded", "false");
      clearDropdown(1);
      clearDropdown(2);
      if (trapped) {
        trapped.onClose();
      }
      return;
    }
  };

  /**
   * Clears specific (given) level dropdown.
   *
   * @param {string} level selector for The dropdown level to close.
   */
  const deleteBreadcrumbText = (level) => {
    const breadcrumbTrigger = menuBreadcrumbs.querySelector(`.${level} button`);
    const breadCrumbText = breadcrumbTrigger.querySelector(`span`);
    if (breadCrumbText) {
      setTimeout(() => {
        breadcrumbTrigger.setAttribute("aria-label", "");
        breadCrumbText.remove();
      }, 100);
    }
  };

  /**
   * Clears specific (given) level dropdown.
   *
   * @param {number} level The dropdown level to close.
   * @param {boolean} refocus If the keyboard focus needs to be refocused on predefined item.
   */
  const clearDropdown = (level, refocus = false) => {
    if (level == 1) {
      menuBreadcrumbs.classList.remove(
        menuClasses.breadcrumbActive,
        menuClasses.breadcrumbActiveLevelOne
      );
      body.classList.remove(
        bodyClasses.subMenuExpanded,
        bodyClasses.subMenuExpandedTransition
      );
      deleteBreadcrumbText(menuClasses.breadcrumbLevelOne);
      const openDropdown = mainMenu.querySelector(
        `.nav__item.${menuClasses.levelZeroOpen}`
      );
      if (openDropdown) {
        openDropdown.classList.remove(
          menuClasses.levelZeroOpen,
          menuClasses.levelZeroOpenTransition
        );
      }
      if (refocus) {
        setTimeout(() => {
          const firstLink = mainMenu.querySelector(`.nav__item a`);
          firstLink.focus();
        }, 500);
      }
      return;
    }
    if (
      level == 2 ||
      (level == 1 &&
        menuBreadcrumbs.classList.contains(
          menuClasses.breadcrumbActiveLevelTwo
        ))
    ) {
      menuBreadcrumbs.classList.remove(menuClasses.breadcrumbActiveLevelTwo);
      const openSubDropdownGroup = mainMenu.querySelector(
        `.nav__dropdown-group.${menuClasses.subMenuGroupActive}`
      );
      deleteBreadcrumbText(menuClasses.breadcrumbLevelTwo);
      if (openSubDropdownGroup) {
        openSubDropdownGroup.classList.remove(menuClasses.subMenuGroupActive);
      }
      const openSubDropdown = mainMenu.querySelector(
        `.nav__dropdown.${menuClasses.levelOneOpen}`
      );
      if (openSubDropdown) {
        openSubDropdown.classList.remove(
          menuClasses.levelOneOpen,
          menuClasses.levelOneOpenTransition
        );
      }
      if (refocus) {
        setTimeout(() => {
          const firstBreadcrumb = document.querySelector(
            `.${menuClasses.breadcrumbLevelOne} button`
          );
          firstBreadcrumb.focus();
        }, 300);
      }
    }
  };

  /**
   * Generates span element with link content and appends it to given breadcrumb level.
   *
   * @param {Element} link The link element to append to breadcrumb.
   * @param {number} level The dropdown level to append link to.
   *
   */
  const generateBreadcrumb = (link, level) => {
    const attachedLevel = menuBreadcrumbs.querySelector(
      `.nav__breadcrumbs-level-${level} > button`
    );
    const linkContent = link.textContent;
    const breadCrumbElem = document.createElement("span");
    breadCrumbElem.textContent = linkContent;
    attachedLevel.appendChild(breadCrumbElem);
    attachedLevel.setAttribute("aria-label", `exit ${linkContent} submenu`);
    menuBreadcrumbs.classList.add(
      menuClasses.breadcrumbActive,
      `breadcrumbs-active--level-${level}`
    );
    setTimeout(() => {
      attachedLevel.focus();
    }, 500);
  };

  /**
   * click event listener for clearing specific breadcrumb level.
   *
   */
  menuBreadcrumbsLevels.forEach((level) => {
    level.addEventListener("click", (e) => {
      if (
        e.target.parentNode.classList.contains(menuClasses.breadcrumbLevelOne)
      ) {
        clearDropdown(1, true);
      } else {
        clearDropdown(2, true);
      }
    });
  });

  /**
   * click event listener for level 0 dropdown carets.
   */
  dropdownCarets.forEach((caret) => {
    caret.addEventListener("click", (e) => {
      if (body.classList.contains(bodyClasses.subMenuExpanded)) {
        body.classList.remove(
          bodyClasses.subMenuExpanded,
          bodyClasses.subMenuExpandedTransition
        );
        return;
      }

      const dropdownParent = caret.closest(".nav__item");
      const levelZeroLink = dropdownParent.querySelector(".nav__link > a");
      body.classList.add(bodyClasses.subMenuExpanded);
      dropdownParent.classList.add(menuClasses.levelZeroOpen);
      setTimeout(() => {
        dropdownParent.classList.add(menuClasses.levelZeroOpenTransition);
        if (levelZeroLink) {
          generateBreadcrumb(levelZeroLink, 1);
        }
      }, 200);
      setTimeout(() => {
        body.classList.add(bodyClasses.subMenuExpandedTransition);
      }, 800);
    });
  });

  /**
   * click event listener for level 1 dropdown carets.
   */
  subCaretDropdown.forEach((subCaret) => {
    subCaret.addEventListener("click", (e) => {
      const subNavParent = e.target.closest(".nav__dropdown-group");
      const contentWrapper = e.target.closest(".nav__dropdown");

      if (!subNavParent) {
        return;
      }

      const subNavParentLink = subNavParent.querySelector(
        ".nav__dropdown-group-link"
      );
      if (subNavParentLink) {
        generateBreadcrumb(subNavParentLink, 2);
        contentWrapper.classList.add(menuClasses.levelOneOpen);
        subNavParent.classList.add(menuClasses.subMenuGroupActive);
        setTimeout(() => {
          contentWrapper.classList.add(menuClasses.levelOneOpenTransition);
        }, 150);
      }
    });
  });

  /**
   * click event listener for mobile hamburger menu trigger icon.
   */
  hamburgerMenu.addEventListener("click", () => {
    if (!body.classList.contains(bodyClasses.mobileNavOpen)) {
      body.classList.add(bodyClasses.mobileNavOpen);
      hamburgerMenu.setAttribute("aria-expanded", "true");
      trapped = trapFocus(mainMenu);

      // We need animations enabled for the menu to work.
      if (localStorage.getItem("reduce_animation") === "true") {
        body.classList.add("enable-motion");
        body.classList.remove("disable-motion");
      }
    }
  });

  /**
   * click event listener for mobile menu close icon.
   */
  closeIcon.addEventListener("click", closeMenu);

  /**
   * MediaQueryList change event to close menu when switching back to desktop breakpoint.
   */
  sizeMq.addEventListener("change", (event) => {
    if (event.matches) {
      closeMenu();
    }
  });
};
