import { initializeStatisticsComponent } from "./statistics.js";
import "./statistics.scss";

((Drupal) => {
  Drupal.behaviors.statistics = {
    attach: (context) => {
      once("statistics", context.querySelectorAll(".c-statistics")).forEach(
        () => {
          initializeStatisticsComponent();
        }
      );
    },
  };
})(Drupal);
