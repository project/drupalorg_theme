export const initializeStatisticsComponent = () => {
  const component = document.querySelector(".c-statistics");

  if (!component) {
    return;
  }

  gsap.registerPlugin(ScrollTrigger);

  const mediaWrapper = component.querySelector(".c-statistics__image-inner");
  const mediaImage = component.querySelector("img");

  const timelineOptions = {
    scrub: true,
  };

  const desktopBreakpoint = 1024;
  const mm = gsap.matchMedia();

  mm.add(
    {
      isDesktop: `(min-width: ${desktopBreakpoint}px)`,
      isMobile: `(max-width: ${desktopBreakpoint - 1}px)`,
    },
    (context) => {
      const { isDesktop } = context.conditions;

      if (isDesktop) {
        const inset = gsap.timeline({
          scrollTrigger: {
            trigger: mediaWrapper,
            start: "top+=100 bottom",
            end: "top+=200",
            toggleClass: "active",
            ...timelineOptions,
          },
        });

        const scale = gsap.timeline({
          scrollTrigger: {
            trigger: mediaWrapper,
            start: "top+=250 bottom",
            end: "top+=120",
            ...timelineOptions,
          },
        });

        const opacity = gsap.timeline({
          scrollTrigger: {
            trigger: mediaWrapper,
            start: "top+=100 bottom",
            end: "top+=200 top+=200",
            ...timelineOptions,
          },
          onComplete: () => {
            component.classList.add("content-visible");
          },
          onReverseComplete: () => {
            component.classList.remove("content-visible");
          },
        });

        inset.fromTo(
          mediaWrapper,
          {
            clipPath: "inset(0 5% round 60px)",
          },
          {
            clipPath: "inset(0 0% round 0px)",
          }
        );

        scale.to(mediaImage, {
          y: "-2%",
          scale: 1.08,
        });

        opacity.fromTo(
          mediaWrapper,
          {
            "--overlay-opacity": 0,
          },
          {
            "--overlay-opacity": 0.3,
          }
        );
      }
    }
  );

  const slider = component.querySelector(".swiper");
  if (!slider) {
    return;
  }

  new Swiper(slider, {
    mousewheel: false,
    speed: 700,
    allowTouchMove: true,
    autoHeight: true,
    rewind: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
  });
};
