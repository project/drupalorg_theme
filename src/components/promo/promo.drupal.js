import { initializePromo } from "./promo.js";

import "./promo.scss";

((Drupal) => {
  Drupal.behaviors.promo = {
    attach: (context) => {
      once("promo", context.querySelectorAll(".c-promo"), context).forEach(
        () => {
          initializePromo();
        }
      );
    },
  };
})(Drupal);
