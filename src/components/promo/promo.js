export const initializePromo = () => {
  const promoComponent = document.querySelector(".c-promo");

  if (!promoComponent) {
    return;
  }

  const promoComponentQuote = promoComponent.querySelector(
    ".c-promo__quote-wrapper"
  );

  const lastFragment = promoComponent.querySelector(
    ".c-promo__inner > .field__items > .field__item:last-child"
  );

  if (!lastFragment.querySelector(".c-promo__quote-wrapper")) {
    lastFragment.appendChild(promoComponentQuote.cloneNode(true));
  }
};
