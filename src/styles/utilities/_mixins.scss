@use '../settings/variables' as *;

/**
 * Breakpoints - Min and Max-widths for responsive design.
 * Accepts:
 * @param {String} - $breakpoint Name from variable breakpoint map.
 * @param {String} $minmax [min] - Either min (for min-width) or max (for max-width).
 *
 * Usage:
 * .thing {
 *   @include bp(md) { display: none }
 * }
 */
@mixin bp($breakpoint, $minmax: 'min') {
  @if map-has-key($breakpoints, $breakpoint) {
    @if $minmax == 'min' {
      @media (min-width: #{map-get($breakpoints, $breakpoint)}) {
        @content;
      }
    } @else if $minmax == 'max' {
      @media (max-width: #{map-get($breakpoints, $breakpoint) - 1px}) {
        @content;
      }
    } @else {
      @warn 'Media query not supported.';
    }
  } @else {
    @warn "Breakpoint doesn't exist please check breakpoints map.";
  }
}

/**
 * Gradients
 * Accepts:
 * - $start-color: {String} Color variable.
 * - $end-color {String} Color variable.
 * - $orientation { String } vertical, horizonal, null.
 *
 * Usage:
 * .thing {
 *   @include gradient(red, blue, horizontal);
 * }
 */
@mixin gradient($start-color, $end-color, $orientation) {
  background: $start-color;

  @if $orientation == 'vertical' {
    background: linear-gradient(to bottom, $start-color, $end-color);
  } @else if $orientation == 'horizontal' {
    background: linear-gradient(to right, $start-color, $end-color);
  } @else if $orientation == 'radial' {
    background: radial-gradient(ellipse at center, $start-color, $end-color);
  } @else {
    @warn 'Invalid orientation';
  }
}

/**
 * Is active
 * Helper for native states
 *
 * Usage:
 * .thing {
 *   @include is-active;
 * }
 */
@mixin is-active {
  &:hover,
  &:active,
  &:focus {
    @content;
  }
}

/**
 * State is...
 * For when state changes are made with js.
 * Accepts:
 * - $el: {String} Class added by js.
 * - $position: {String} Where is lies on the DOM.
 * - $parent: {String} Parent element (optional).
 *
 * Usage:
 * .thing::before {
 *   @include state-is('active', 'self');
 * }
 */
@mixin state-is($el, $position: 'self', $parent: '') {
  @if $position == 'self' {
    #{$parent} &#{$el} {
      @content;
    }
  } @else if $position == 'child' {
    #{$parent} & #{$el} {
      @content;
    }
  } @else if $position == 'parent' {
    #{$parent} #{$el} & {
      @content;
    }
  } @else {
    @warn 'Invalid position';
  }
}

/**
 * Container - great for creating constrained layouts
 * Accepts:
 * - $type: {String} Container Type Variable.
 *
 * Usage:
 * .thing {
 *   @include container(type);
 * }
*/
@mixin container($type: $container-default) {
  @if map-has-key($containers, $type) {
    padding-right: $container-padding-m;
    padding-left: $container-padding-m;

    @each $breakpoint, $padding in map-get($containers, $type) {
      @include bp($breakpoint) {
        padding-right: $padding;
        padding-left: $padding;
      }
    }
  } @else {
    @error 'Invalid container type, please check container variables.';
  }
}

/**
 * Simple text clamp.
 *
 * Usage:
 * .thing {
 *   @include text-clamp(lines);
 * }
 */
/* stylelint-disable value-no-vendor-prefix */
/* stylelint-disable property-no-vendor-prefix */
@mixin text-clamp($lines: 2) {
  display: -webkit-box;
  overflow: hidden;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: $lines;
}

/**
 * Backdrop Blur
 *
 * Usage:
 * .thing {
 *   @include backdrop-blur(length);
 * }
 */

@mixin backdrop-blur(
  $length: 10px,
  $background: rgba($c-white, 0.4),
  $fallback: $c-white
) {
  background: $fallback;

  @supports ((-webkit-backdrop-filter: none) or (backdrop-filter: none)) {
    background: rgba($c-white, 0.4);
    -webkit-transform: translate3d(0, 0, 0);
    -webkit-backdrop-filter: blur($length);
    backdrop-filter: blur($length);
  }
}
/* stylelint-enable value-no-vendor-prefix */
/* stylelint-enable property-no-vendor-prefix */

/**
 * Data Reveal Global Animation
 *
 * Usage:
 * .thing {
 *   @include data-reveal(translate, skew);
 * }
*/
@mixin data-reveal($translate: 80%, $skew: 3deg, $duration: 0.8) {
  .enable-motion & {
    transform: translateY(#{$translate}) skewY(#{$skew});
    visibility: visible;
    opacity: 0;
    transition: opacity #{$duration}s $t-cubic-easing,
      visibility #{$duration}s $t-cubic-easing,
      transform #{$duration}s $t-cubic-easing;
    will-change: transform, opacity;
  }
}

/**
 * Stagerred content reveal
 *
 * Usage:
 * .thing {
 *   @include stagger-reveal(initial, increase, count);
 * }
*/
@mixin stagger-reveal(
  $initial: 0,
  $increase: 0.05,
  $count: 20,
  $selector: null
) {
  @for $i from 1 through $count {
    $initial: $initial + $increase;
    @if ($selector) {
      &:nth-child(#{$i}) #{$selector} {
        transition-delay: #{$initial}s;
      }
    } @else {
      &:nth-child(#{$i}) {
        transition-delay: #{$initial}s;
      }
    }
  }
}

/**
 * 'draw' underline animation
 *
 * Usage:
 * .thing {
 *   @include underline(color, height, spacing);
 * }
 */
@mixin underline($color: currentColor, $height: 2px, $spacing: 3px) {
  padding-bottom: $spacing;
  text-decoration: none;
  background-image: linear-gradient($color, $color);
  background-repeat: no-repeat;
  background-position: 100% 100%;
  background-size: 0% $height;
  transition: background-size 0.3s ease-in-out;

  &:hover,
  &:focus-visible {
    background-position: 0 100%;
    background-size: 100% $height;
  }
}

/**
 * 'Undraw' underline animation
 *
 * Usage:
 * .thing {
 *   @include underline-reverse(color, height, spacing);
 * }
 */
@mixin underline-reverse($color: currentColor, $height: 2px, $spacing: 3px) {
  padding-bottom: $spacing;
  text-decoration: none;
  background-image: linear-gradient($color, $color);
  background-repeat: no-repeat;
  background-position: 0 100%;
  background-size: 100% $height;
  transition: background-size 0.3s ease-in-out;

  &:hover,
  &:focus-visible {
    background-position: 100% 100%;
    background-size: 0% $height;
  }

  @media (prefers-reduced-motion) {
    transition: none;
  }
}

/**
 * List reset.
 *
 * Usage:
 * .thing {
 *   @include list-reset;
 * }
 */
@mixin list-reset {
  margin: 0;
  padding: 0;
  list-style: none;
}

/**
 * Button reset.
 *
 * Resets padding, background, border, and cursor for a button element.
 *
 * Usage:
 * .btn {
 *   @include button-reset;
 * }
 */
@mixin button-reset {
  padding: 0;
  background: transparent;
  border: 0;
  cursor: pointer;
}

/**
 * Dark header.
 *
 * Applies styles to elements whenever header is supposed to be dark colored.
 *
 * Usage:
 * @include dark-header {
 *   @content;
 * }
 * @TODO: remove instances of dark-header
 */
@mixin dark-header {
  .INVALIDpath-layout-builder-page,
  .INVALIDsite-header.site-header--scrolled,
  .INVALIDsearch-expanded,
  .INVALIDpath-frontpage {
    @content;
  }
}

// Mixins brought from Bluecheese.

@mixin large-button {
  font-size: (1.4em);  // 18px
  font-weight: normal;
  color: $c-white;
  background-color: $c-green;
  border: 2px solid $c-green;

  &:hover,
  &:focus {
    background-color: $c-medium-green;
    border-color: $c-medium-green;
    cursor: pointer;
  }
}

@mixin medium-button {
  color: $c-white;
  background-color: $c-green;
  border: 2px solid $c-green;

  &:hover,
  &:focus {
    background-color: $c-medium-green;
    border-color: $c-medium-green;
    cursor: pointer;
  }
}

@mixin clearfix {
  &::after {
    content: "";
    display: block;
    clear: both;
  }
}
