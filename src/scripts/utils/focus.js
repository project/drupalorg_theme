export const trapFocus = (element, initialFocus = document.activeElement) => {
  const allFocusableElements = [
    ...element.querySelectorAll(
      'a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled])'
    ),
  ];

  const firstFocusableEl = allFocusableElements[0];
  const lastFocusableEl = allFocusableElements[allFocusableElements.length - 1];

  let currentFocus = null;

  setTimeout(() => {
    firstFocusableEl.focus();
  }, 500);

  currentFocus = firstFocusableEl;

  const handleFocus = (e) => {
    e.preventDefault();
    if (allFocusableElements.includes(e.target)) {
      currentFocus = e.target;
      return;
    }

    if (currentFocus === firstFocusableEl) {
      lastFocusableEl.focus();
    } else {
      firstFocusableEl.focus();
    }

    currentFocus = document.activeElement;
  };

  document.addEventListener("focus", handleFocus, true);

  return {
    onClose: () => {
      document.removeEventListener("focus", handleFocus, true);
      initialFocus.focus();
    },
  };
};
