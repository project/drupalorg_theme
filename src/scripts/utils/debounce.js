/**
 * Creates a debounced function that delays invoking the given function until after a specified time has elapsed
 * since the last time the debounced function was invoked.
 *
 * @param {Function} func - The function to be debounced.
 * @param {number} delay - The delay in milliseconds before invoking the function after the last call.
 * @returns {Function} - The debounced function.
 */
export const debounce = (func, delay) => {
  let timeoutId;

  return function (...args) {
    clearTimeout(timeoutId);

    timeoutId = setTimeout(() => {
      func.apply(this, args);
    }, delay);
  };
};
