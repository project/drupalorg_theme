import path from "path";
import { fileURLToPath } from "url";
import aliasImporter from "node-sass-alias-importer";
import alias from "@rollup/plugin-alias";
import sass from "rollup-plugin-sass";
import postcss from "postcss";
import postcssPlugin from "postcss-minify";
import terser from "@rollup/plugin-terser";
import strip from "@rollup/plugin-strip";
import copy from "rollup-plugin-copy";
import pkg from "fast-glob";
const { sync } = pkg;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const entries = sync("src/**/*.drupal.js", {});

const ledger = entries.map((entry) => {
  return {
    input: entry,
    output: entry.replace("src", "dist").replace(".drupal.js", ".js"),
  };
});

const aliases = {
  "@styles": path.resolve(__dirname, "src/styles/"),
  "@scripts": path.resolve(__dirname, "src/scripts"),
  "@components": path.resolve(__dirname, "src/components"),
  "@fonts": path.resolve(__dirname, "src/assets/fonts"),
};

const source = ledger.map(({ input, output }) => {
  return {
    input,
    output: {
      file: output,
      format: "iife",
    },
    plugins: [
      alias({ resolve: [".ts", ".js"], entries: aliases }),
      terser(),
      sass({
        output: true,
        options: {
          importer: aliasImporter(aliases, {
            root: ".",
          }),
        },
        processor: (css) =>
          postcss([postcssPlugin])
            .process(css)
            .then((result) => result.css),
      }),
      strip({
        include: "**/*.(mjs|js|ts)",
        functions: [],
      }),
      copy({
        targets: [{ src: "src/assets/*", dest: "dist/assets" }],
      }),
    ],
  };
});

export default source;
