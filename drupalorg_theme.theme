<?php

/**
 * @file
 * Functions to support theming in the drupalorg theme and subthemes.
 */

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_theme_suggestions_HOOK_alter()
 */
function drupalorg_theme_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  if (isset($variables['elements']['content']['#block_content'])) {
    $suggestions[] = 'block__' . $variables['elements']['content']['#block_content']->bundle();
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function drupalorg_theme_theme_registry_alter(&$theme_registry) {
  $theme_keys = [
    'block',
    'html',
    'media',
    'node',
    'page',
    'paragraph',
    'region',
    'taxonomy_term',
    'view',
  ];

  // Run through each theme id.
  foreach (array_keys($theme_registry) as $key) {
    // Attach post process if key or base_hook is in supported theme keys.
    if (in_array($key, $theme_keys) || (array_key_exists('base hook', $theme_registry[$key]) && in_array($theme_registry[$key]['base hook'], $theme_keys))) {
      // Add postprocess function to automatically convert attributes arrays.
      $theme_registry[$key]['preprocess functions'][] = 'drupalorg_theme_postprocess';
    }
  }
}

/**
 * Custom postprocess function added to cleanup preprocess variables.
 *
 * @see \Drupal\Core\Theme\ThemeManager::render()
 */
function drupalorg_theme_postprocess(array &$variables) {
  // Initialize attribute variables if not set.
  if (!isset($variables['#attribute_variables']) || !is_array($variables['#attribute_variables'])) {
    $variables['#attribute_variables'] = [];
  }

  // Combine attribute variables with some default variables.
  $default_attribute_variables = [
    'figcaption_attributes',
    'footer_attributes',
    'image_attributes',
    'inner_attributes',
  ];
  $variables['#attribute_variables'] = array_merge($variables['#attribute_variables'], $default_attribute_variables);

  // Convert attribute variables to Attribute objects for rendering.
  $default_attributes = new Attribute();
  foreach ($variables['#attribute_variables'] as $key) {
    if (isset($variables[$key]) && !($variables[$key] instanceof Attribute)) {
      // If any values are set, convert to an Attribute.
      if (isset($variables[$key]) && $variables[$key]) {
        $variables[$key] = new Attribute($variables[$key]);
      }
      // Otherwise, clone from defaults.
      else {
        $variables[$key] = clone $default_attributes;
      }
    }
  }
}

/**
 * Implements hook_preprocess_hook() for block.
 */
function drupalorg_theme_preprocess_block(array &$variables) {
  $plugin_css = Html::cleanCssIdentifier($variables['plugin_id']);
  $provider_css = Html::cleanCssIdentifier($variables['configuration']['provider']);
  $block_class = 'block-' . $plugin_css;

  // Attach default and bundle-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'block', $provider_css, $plugin_css);

  // Basic attribute variables.
  $variables['attributes']['class'][] = 'block';
  $variables['attributes']['class'][] = 'block-' . $provider_css;
  $variables['attributes']['class'][] = $block_class;
  $variables['attributes']['data-entity-id'] = $variables['plugin_id'];
  $variables['attributes']['data-entity-type'] = 'block';

  $variables['content_attributes']['class'][] = 'block__content';
  $variables['content_attributes']['class'][] = $block_class . '__content';

  $variables['title_attributes']['class'][] = 'title';
  $variables['title_attributes']['class'][] = 'block__title';
  $variables['title_attributes']['class'][] = $block_class . '__title';
}

/**
 * Implements template_preprocess_HOOK().
 */
function drupalorg_theme_theme_preprocess_block__statistics(&$variables) {
  if (!isset($variables['elements']['content']['#block_content'])) {
    return;
  }
  $block = $variables['elements']['content']['#block_content'];

  $statistics = [];
  if ($block->hasField('field_statistic_items') && !empty($block->get('field_statistic_items'))) {
    $entities = $block->get('field_statistic_items')->referencedEntities();

    foreach ($entities as $entity) {
      $statistic = [];
      if ($entity->hasField('field_value') && !empty($entity->get('field_value')->value)) {
        $statistic['value'] = $entity->get('field_value')->getString();
      }
      if ($entity->hasField('field_unit') && !empty($entity->get('field_unit')->value)) {
        $statistic['unit'] = $entity->get('field_unit')->getString();
      }
      if ($entity->hasField('field_info') && !empty($entity->get('field_info')->value)) {
        $statistic['info'] = $entity->get('field_info')->getString();
      }
      $statistics[] = $statistic;
    }
    $variables['statistics'] = $statistics;
  }
}

/**
 * Implements template_preprocess_HOOK().
 */
function drupalorg_theme_preprocess_block__callout(&$variables) {
  $block = $variables['elements']['content']['#block_content'];
  if ($block->hasField('field_highlight_text') && !empty($block->get('field_highlight_text')->getValue())) {
    $callout_heading = $block->get('field_heading')?->getValue()[0]['value'];
    $heading_highlight = $block->get('field_highlight_text')?->getValue()[0]['value'];
    if (strpos($callout_heading, $heading_highlight) !== FALSE) {
      $variables['highlight_heading'] = $heading_highlight;
    }
  }
}

/**
 * Implements template_preprocess_HOOK().
 */
function drupalorg_theme_preprocess_paragraph__promo_fragment(&$variables) {
  $paragraph = $variables['paragraph'];
  if ($paragraph->hasField('field_highlight_text') && !empty($paragraph->get('field_highlight_text')->getValue())) {
    $heading = $paragraph->get('field_text_fragment')->getValue()[0]['value'];
    $highlight = $paragraph->get('field_highlight_text')->getValue()[0]['value'];
    if (strpos($heading, $highlight) !== FALSE) {
      $variables['highlight'] = $highlight;
    }
  }
}

/**
 * Implements hook_preprocess_hook() for html.
 */
function drupalorg_theme_preprocess_html(array &$variables) {
  $current_path = \Drupal::service('path.current')->getPath();
  $alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);

  // Set class if user is logged in.
  if (isset($variables['logged_in']) && $variables['logged_in']) {
    $variables['attributes']['class'][] = 'user-logged-in';
  }

  // Set class if on layout builder page.
  if (preg_match('/node\/[0-9]+\/layout/', $alias)) {
    $variables['attributes']['class'][] = 'path-layout-builder-page';
  }

  // Set class of the current root path.
  if (isset($variables['root_path']) && !empty($variables['root_path'])) {
    $variables['attributes']['class'][] = 'path-' . Html::cleanCssIdentifier($variables['root_path']);
  }
  else {
    $variables['attributes']['class'][] = 'path-frontpage';
  }
  // Set node type if on node.
  if (isset($variables['node_type']) && !empty($variables['node_type'])) {
    $variables['attributes']['class'][] = 'page-node-type-' . Html::cleanCssIdentifier($variables['node_type']);
  }
  // Alert if database is offline.
  if (isset($variables['db_offline']) && $variables['db_offline']) {
    $variables['attributes']['class'][] = 'db-offline';
  }
}

/**
 * Implements hook_preprocess_hook() for media.
 */
function drupalorg_theme_preprocess_media(array &$variables) {
  /** @var \Drupal\media\MediaInterface $media */
  $media = $variables['media'];
  $bundle = $media->bundle();
  $view_mode = $variables['view_mode'];

  $bundle_css = Html::cleanCssIdentifier($bundle);
  $view_mode_css = Html::cleanCssIdentifier($view_mode);

  // Attach default and bundle-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'media', $view_mode_css, $bundle_css);

  $variables['attributes']['class'][] = 'media';
  $variables['attributes']['class'][] = "media--type-{$bundle_css}";
  $variables['attributes']['class'][] = "media--view-mode-{$view_mode_css}";

  if ($media->isPublished() == FALSE) {
    $variables['attributes']['class'][] = 'media--unpublished';
  }
}

/**
 * Implements hook_preprocess_hook() for node.
 */
function drupalorg_theme_preprocess_node(array &$variables) {
  /** @var \Drupal\node\NodeInterface $node */
  $node = $variables['node'];
  $bundle = $node->bundle();
  $view_mode = $variables['view_mode'];

  $bundle_css = Html::cleanCssIdentifier($bundle);
  $view_mode_css = Html::cleanCssIdentifier($view_mode);

  // Attach default and bundle-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'node', $view_mode_css, $bundle_css);

  // Initialize attributes.
  $variables['footer_attributes'] = [];
  $variables['image_attributes'] = [];

  // Basic attribute variables.
  $variables['attributes']['class'][] = 'node';
  $variables['attributes']['class'][] = "node--type-{$bundle_css}";
  $variables['attributes']['class'][] = "node--view-mode-{$view_mode_css}";
  if ($node->isPromoted()) {
    $variables['attributes']['class'][] = 'node--promoted';
  }
  if ($node->isSticky()) {
    $variables['attributes']['class'][] = 'node--sticky';
  }
  if ($node->isPublished() == FALSE) {
    $variables['attributes']['class'][] = 'node--unpublished';
  }
  $variables['attributes']['data-entity-id'] = $node->id();
  $variables['attributes']['data-entity-type'] = $node->getEntityTypeId();
  $variables['content_attributes']['class'][] = 'node__content';
  $variables['footer_attributes']['class'][] = 'node__footer';
  $variables['footer_attributes']['class'][] = 'node___meta';
  $variables['image_attributes']['class'][] = 'node__image';
  $variables['title_attributes']['class'][] = 'node__title';
}

/**
 * Implements hook_preprocess_hook() for page.
 */
function drupalorg_theme_preprocess_page(array &$variables) {
  if (empty($variables['page']['global'])) {
    // Force the region template to render as there are some global elements needed.
    $variables['page']['global'] = [
      '#theme_wrappers' => ['region'],
      '#region' => 'global'
    ];
  }
  // Track variables that should be converted to attribute objects.
  $variables['#attribute_variables'][] = 'footer_content_attributes';

  // Basic attribute variables.
  $variables['attributes']['class'][] = 'page';
  $variables['footer_attributes']['class'][] = 'region-footer';
  $variables['footer_content_attributes']['class'][] = 'region-footer__content';

  // Use for announcement bar time check.
  $variables['#attached']['drupalSettings']['request_time'] = \Drupal::time()->getRequestTime();

  // Add variable for site status message (for development sites).
  $site_status = \Drupal::config('drupalorg')->get('site_status');
  if ($site_status) {
    $variables['drupalorg_site_status'] = Xss::filterAdmin($site_status);
  }
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function drupalorg_theme_theme_suggestions_field_alter(&$suggestions, array $variables) {
  $element = $variables['element'];

  $no_wrappers = [
    'field_case_study_logo',
    'field_media_image',
  ];

  if (!empty($element['#field_name']) && in_array($element['#field_name'], $no_wrappers)) {
    $suggestions[] = 'field__no_wrappers';
  }
}

/**
 * Implements hook_preprocess_hook() for paragraph.
 */
function drupalorg_theme_preprocess_paragraph(array &$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];
  $bundle = $paragraph->bundle();
  $view_mode = $variables['view_mode'];

  $bundle_css = Html::cleanCssIdentifier($bundle);
  $view_mode_css = Html::cleanCssIdentifier($view_mode);

  // Attach default and bundle-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'paragraph', $view_mode_css, $bundle_css);

  // Set paragraph ID.
  $variables['attributes']['id'] = 'paragraph-' . $paragraph->id();

  // Set default variables and attributes.
  $base_class = 'component-' . $bundle_css;
  $variables['component_base_class'] = $base_class;
  $variables['attributes']['class'][] = $base_class;
  $variables['attributes']['class'][] = 'paragraph--type-' . $bundle_css;
  $variables['attributes']['class'][] = 'paragraph--view-mode-' . $view_mode_css;
  $variables['attributes']['class'][] = 'paragraph-component';
  $variables['attributes']['data-entity-id'] = $paragraph->id();
  $variables['attributes']['data-entity-type'] = $paragraph->getEntityTypeId();
  $variables['image_attributes']['class'][] = $base_class . '__image';
  $variables['title_attributes']['class'][] = $base_class . '__title';
  $variables['content_attributes']['class'][] = $base_class . '__content';
  $variables['footer_attributes']['class'][] = $base_class . '__footer';

  // Set universal reusable variables.
  $variables['image'] = [];
  $variables['title'] = [];
  $variables['footer'] = [];
}

/**
 * Implements hook_preprocess_region().
 */
function drupalorg_theme_preprocess_region(&$variables) {
  $region_css = Html::cleanCssIdentifier($variables['region']);

  // Attach region-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'region', $region_css);

  $variables['attributes']['class'][] = 'region';
  $variables['attributes']['class'][] = "region-{$region_css}";

  $variables['content_attributes']['class'][] = "region-{$region_css}__content";

  switch ($variables['region']) {
    case 'page_start':
      break;

    case 'header':
      $variables['attributes']['role'] = 'banner';
      break;

    case 'pre_content':
      break;

    case 'content':
      $variables['attributes']['id'] = 'main-content';
      $variables['attributes']['role'] = 'main';
      $variables['attributes']['tabindex'] = '-1';
      break;

    case 'post_content':
      break;

    case 'footer_first':
    case 'footer_second':
    case 'footer_third':
      $variables['attributes']['class'][] = 'subregion';
      break;

    case 'page_end':
      break;
  }

}

/**
 * Implements hook_preprocess_hook() for taxonomy_term.
 */
function drupalorg_theme_preprocess_taxonomy_term(array &$variables) {
  /** @var \Drupal\taxonomy\TermInterface $term */
  $term = $variables['term'];
  $bundle = $term->bundle();
  $view_mode = $variables['view_mode'];

  $bundle_css = Html::cleanCssIdentifier($bundle);
  $view_mode_css = Html::cleanCssIdentifier($view_mode);

  // Attach default and bundle-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'node', $view_mode_css, $bundle_css);

  // Initialize attributes.
  $variables['footer_attributes'] = [];

  // Basic attribute variables.
  $variables['attributes']['class'][] = 'term';
  $variables['attributes']['class'][] = "term--vocabulary-{$bundle_css}";
  $variables['attributes']['class'][] = "term--view-mode-{$view_mode_css}";
  $variables['attributes']['data-entity-id'] = $term->id();
  $variables['attributes']['data-entity-type'] = $term->getEntityTypeId();
  $variables['content_attributes']['class'][] = 'term__content';
  $variables['footer_attributes']['class'][] = 'term__footer';
  $variables['title_attributes']['class'][] = 'term__title';
  if ($variables['page']) {
    $variables['title_attributes']['class'][] = 'visually-hidden';
  }

  // Set universal reusable variables.
  $variables['footer'] = [];
}

/**
 * Implements hook_preprocess_hook() for views_view.
 */
function drupalorg_theme_preprocess_views_view(array &$variables) {
  // Attach default and bundle-specific libraries.
  _drupalorg_theme_attach_libraries($variables, 'view', $variables['id'], $variables['display_id']);

  $variables['attributes']['data-entity-id'] = $variables['id'];
  $variables['attributes']['data-entity-type'] = 'view';
}

/**
 * Implements hook_preprocess_hook() for breadcrumb.
 */
function drupalorg_theme_preprocess_breadcrumb(array &$variables) {
  if (!empty($variables['breadcrumb'])) {
    foreach ($variables['breadcrumb'] as $index => $link) {
      if (empty($link['text'])) {
        unset($variables['breadcrumb'][$index]);
      }
    }
  }

  // Attach default libraries.
  _drupalorg_theme_attach_libraries($variables, 'breadcrumb');
}

/**
 * Auto attaches libraries for active and base themes using naming convention.
 *
 * @param array &$variables
 *   The template variables array to attach libraries to.
 * @param string $prefix
 *   The library prefix to use, typically the entity type ID.
 * @param string $view_mode
 *   The view-mode. Defaults to 'full'.
 * @param null|string $bundle
 *   The entity type bundle or more specific instance ID.
 */
function _drupalorg_theme_attach_libraries(array &$variables, $prefix, $view_mode = 'full', $bundle = NULL) {
  // Clean variables.
  $prefix = Html::cleanCssIdentifier($prefix);
  $view_mode = Html::cleanCssIdentifier($view_mode);
  $bundle = (!is_null($bundle)) ? Html::cleanCssIdentifier($bundle) : $bundle;

  // Get active theme.
  $active_theme = \Drupal::theme()->getActiveTheme();

  // Get themed to attach libraries for (filter out ones we don't care about).
  $theme_names = array_filter(array_keys($active_theme->getBaseThemeExtensions()), function ($theme_name) {
    return !in_array($theme_name, ['drupalorg_theme', 'classy', 'stable']);
  });

  // Add in the current active theme.
  $theme_names[] = $active_theme->getName();

  // Attach dynamic libraries.
  foreach ($theme_names as $theme_name) {
    $variables['#attached']['library'][] = "{$theme_name}/{$prefix}";
    $variables['#attached']['library'][] = "{$theme_name}/{$prefix}--{$view_mode}";
    if (!is_null($bundle)) {
      $variables['#attached']['library'][] = "{$theme_name}/{$prefix}--{$view_mode}--{$bundle}";
    }
  }
}

/**
 * Implements template_preprocess_field().
 */
function drupalorg_theme_preprocess_field__field_why_drupal_slides(&$variables, $hook) {
  foreach ($variables['items'] as $idx => $item) {
    $paragraph = $item['content']['#paragraph'];
    $variables['bundle'] = $paragraph->bundle();
    if ($paragraph->hasField('field_benefit_title') && !empty($paragraph->get('field_benefit_title')->getValue())) {
      $variables['slide_titles'][$idx] = $paragraph->get('field_benefit_title')->getValue()[0];
    }
    if ($paragraph->hasField('field_description') && !empty($paragraph->get('field_description')->getValue())) {
      $variables['slide_descriptions'][$idx] = $paragraph->get('field_description')->getValue()[0];
    }
    if ($paragraph->hasField('field_landing_page_link') && !empty($paragraph->get('field_landing_page_link')->getValue()[0])) {
      $variables['slide_landing_text'][$idx] = $paragraph->get('field_landing_page_link')->getValue()[0]['title'];
      $variables['slide_landing_url'][$idx] = Url::fromUri($paragraph->get('field_landing_page_link')->getValue()[0]['uri']);
    }
    if ($paragraph->hasField('field_documentation_link') && !empty($paragraph->get('field_documentation_link')->getValue()[0])) {
      $variables['slide_documentation_text'][$idx] = $paragraph->get('field_documentation_link')->getValue()[0]['title'];
      $variables['slide_documentation_url'][$idx] = Url::fromUri($paragraph->get('field_documentation_link')->getValue()[0]['uri']);
    }
    if ($paragraph->hasField('field_case_study_image') && !empty($paragraph->get('field_case_study_image')->getValue())) {
      $image = $paragraph->get('field_case_study_image')->referencedEntities()[0];
      $image_media = \Drupal::entityTypeManager()->getViewBuilder('media')->view($image);
      $variables['slide_images'][$idx] = $image_media;
    }
    if ($paragraph->hasField('field_case_study_link') && !empty($paragraph->get('field_case_study_link')->getValue()[0])) {
      $variables['slide_case_study_text'][$idx] = $paragraph->get('field_case_study_link')->getValue()[0]['title'];
      $variables['slide_case_study_url'][$idx] = Url::fromUri($paragraph->get('field_case_study_link')->getValue()[0]['uri']);
    }
  }
}
