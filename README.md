# Drupal.org front end build system - Project Setup Documentation (WIP)

## Overview

This project utilizes pnpm as the package manager and Rollup as the module bundler. It is designed to handle JavaScript and SCSS files using Rollup for efficient bundling.

## Prerequisites

Make sure you have Node.js and pnpm installed on your machine.

- [Node.js](https://nodejs.org/)
- [pnpm](https://pnpm.io/)

## Installation

# Install dependencies using pnpm
`pnpm install`

## Usage

## Running Rollup

To bundle your JavaScript and CSS files, use the following npm scripts:

- Run Rollup once:
`npm run rollup`

- Watch for changes and run Rollup automatically:
`npm run rollup:watch`

### Project Structure

```plaintext
drupalorg_theme (Theme)
│
├── src
│   ├── assets
│   │   ├── js
│   │   ├── css
│   │   ├── svg
│   │   ├── images
│   │   └── fonts
│   ├── components
│   │   ├── component1
│   │   │   ├── component1.js
│   │   │   ├── component1.drupal.js
│   │   │   └── component1.scss
│   │   ├── component2
│   │   │   ├── component2.js
│   │   │   ├── component2.drupal.js
│   │   │   └── component2.scss
│   │   └── ...
│   ├── scripts
│   │   ├── utils
│   │   │   ├── util1.js
│   │   │   ├── util2.js
│   │   │   └── ...
│   └── styles
│       ├── global.scss
│       └── ...
├── .stylelintrc
├── rollup.config.js
└── ...
